import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PokemonServiceService } from './Services/pokemon-service.service';
import { DetailPokemonComponent } from './detail-pokemon/detail-pokemon.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Pokedex';
  pokemons: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource <any>;
  displayedColumns: string[] = ['nom','detail'];
  constructor(private pokemonServiceService:PokemonServiceService,private dialog: MatDialog,){}
  ngOnInit() {
    this.getAllPokemon();
  }
  getAllPokemon(){
    this.pokemonServiceService.getAllPokemon().then((data =>{
      this.dataSource = new MatTableDataSource(data.results);
      this.dataSource.paginator = this.paginator;
    })).catch(e=>{
      console.log('Erreur',e);
    });
  }
  OpenPokemonDialog(url :string){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      url: url
    };
    const dialogRef=this.dialog.open(DetailPokemonComponent, dialogConfig);
  }
}
