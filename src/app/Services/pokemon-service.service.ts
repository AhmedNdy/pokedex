import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PokemonServiceService {

  constructor(private http: HttpClient) { }
  getAllPokemon(): Promise<any> {
     return this.http.get(environment.serverUrl ,
     {}).toPromise().then((reponse: any) => {
        return Promise.resolve(reponse);
     });
    
}
getPokemonDetail(url: string):Promise<any> {
    return this.http.get(url ,
    {}).toPromise().then((reponse: any) => {
       return Promise.resolve(reponse);
    });
}
}
