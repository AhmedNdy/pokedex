import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PokemonServiceService } from '../Services/pokemon-service.service';


@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styleUrls: ['./detail-pokemon.component.css']
})
export class DetailPokemonComponent implements OnInit {
url: string;
type:string;
  constructor(public dialogRef: MatDialogRef<DetailPokemonComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private pokemonServiceService:PokemonServiceService) {
      this.url = data['url'];
    
      this.pokemonServiceService.getPokemonDetail(this.url).then((response)=>
      {
        this.type = response.types[1].type.name;
      }).catch((e)=>{
        console.log('Erreur',e);
      });
  
     }

  ngOnInit(): void {
  }

}
